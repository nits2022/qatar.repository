﻿using AutoMapper;
using CSharpFunctionalExtensions;
using MediatR;
using Qatar.DAL.DataModel;
using Qatar.DAL.IRepositories;
using Qatar.Domain.Commands.Users;
using Qatar.Domain.DTO;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Qatar.BAL.CommandHandlers.Users
{
    public class UserCommandHandler : IRequestHandler<CreateUserCommand, UserDto>,
    IRequestHandler<UpdateUserCommand, Result<UserDto>>,
    IRequestHandler<DeleteUserCommand, Result>
    {
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public UserCommandHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<User>(request.Request);

            await _userRepository.InsertAsync(user);

            return request.Request;
        }

        public async Task<Result> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(request.Id);
            if (user == null)
                return Result.Failure($"User not found");
            await _userRepository.DeleteAsync(user);
            return Result.Success();
        }

        public async Task<Result<UserDto>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(request.Request.Id);
            if (user == null)
                throw new ArgumentException("Invalid id");
            _mapper.Map(request.Request, user);
            await _userRepository.UpdateAsync(user);
            return Result.Success(request.Request);
        }
    }
}
