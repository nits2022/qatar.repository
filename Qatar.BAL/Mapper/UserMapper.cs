﻿using AutoMapper;
using Qatar.DAL.DataModel;
using Qatar.Domain.DTO;

namespace Qatar.BAL.Mapper
{
    public class UserMapper: Profile
    {
        public UserMapper()
        {
            CreateMap<UserDto, User>();
            CreateMap<User, UserDto>();
            //CreateMap<List<User>, List<UserDto>>();
            //CreateMap<List<UserDto>, List<User>>();
        }
    }
}
