﻿using AutoMapper;
using MediatR;
using Qatar.DAL.IRepositories;
using Qatar.Domain.DTO;
using Qatar.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Qatar.BAL.QueryHandlers
{
    public class UserQueryHandler : IRequestHandler<UserQuery, (IEnumerable<UserDto> result, int count)>
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public UserQueryHandler(
            IMapper mapper,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<(IEnumerable<UserDto> result, int count)> Handle(UserQuery request, CancellationToken cancellationToken)
        {
            bool isSelectedId = false;
            if (request.SelectedId != null && request.SelectedId > 0)
            {
                request.Filter.Predicates.Add("id", request.SelectedId);
                isSelectedId = true;
            }

            var data = await _userRepository.GetAsync(request.Filter);
            int totalCount = data.Count();
            if (!isSelectedId)
            {
                totalCount = await _userRepository.CountAsync(request.Filter, true);
            }

            var result = _mapper.Map<IEnumerable<UserDto>>(data);
            return (result, totalCount);
        }
    }
}
