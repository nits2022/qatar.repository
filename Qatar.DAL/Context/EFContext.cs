﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qatar.DAL.Context
{
    public class EFContext : IEFContext
    {
        private DbContext _context;

        public EFContext(ILoggerFactory loggerFactory, DbContext context)
        {
            _context = context;
        }

        public DbContext Context
        {
            get
            {
                return _context;
            }
        }

        public void Dispose()
        {
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
