﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qatar.DAL.Context
{
    public interface IEFContext : IDisposable
    {
        DbContext Context { get; }

        void SaveChanges();
    }
}
