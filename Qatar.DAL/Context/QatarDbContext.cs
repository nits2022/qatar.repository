﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Qatar.DAL.Configurations;

namespace Qatar.DAL.Context
{
    public class QatarDbContext : DbContext
    {
        private readonly ILoggerFactory _loggerFactory;

        public QatarDbContext(
            ILoggerFactory loggerFactory,
            DbContextOptions<QatarDbContext> options)
            : base(options)
        {
            _loggerFactory = loggerFactory;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(_loggerFactory);
            optionsBuilder.EnableSensitiveDataLogging(true);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
