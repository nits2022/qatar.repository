﻿using MediatR;
using Microsoft.Extensions.Logging;
using Qatar.DAL.Context;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Qatar.DAL.Handlers
{
    public class TransactionBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>, ITransactionalRequest
    {
        private readonly ILogger<TransactionBehaviour<TRequest, TResponse>> _logger;
        private readonly IEFContext _context;


        public TransactionBehaviour(IEFContext context,
            ILogger<TransactionBehaviour<TRequest, TResponse>> logger)
        {
            _context = context ??
                throw new ArgumentException(nameof(IEFContext));
            _logger = logger ??
                throw new ArgumentException(nameof(ILogger));

        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {

            try
            {
                _logger.LogInformation($"Begin transaction {typeof(TRequest).Name}");

                // awit _context.BeginTransactionAsync();
                var response = await next();

                // await _context.CommitTransactionAsync();
                _context.SaveChanges();
                _logger.LogInformation($"Committed transaction {typeof(TRequest).Name}");

                return response;
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Rollback transaction executed {typeof(TRequest).Name}");

                // _context.RollbackTransaction();

                _logger.LogError(e.Message, e.StackTrace);

                throw e;
            }
        }

    }
}

