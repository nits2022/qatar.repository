﻿using Qatar.DAL.DataModel;
using Qatar.DAL.Implementations;

namespace Qatar.DAL.IRepositories
{
    public interface IUserRepository : IRepository<User>
    {

    }
}
