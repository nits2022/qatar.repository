﻿using Qatar.DAL.Context;
using Qatar.DAL.Implementations;
using Qatar.DAL.Models;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore.Query;

namespace Qatar.DAL
{

    public class BaseRepository<T> : IRepository<T>
   where T : Entity, new()
    {
        private readonly DbContext _context;

        public BaseRepository(IEFContext context)
        {
            _context = context.Context;
        }

        public DbContext Context
        {
            get { return _context; }
        }

        public virtual IQueryable<T> Query
        {
            get { return DbSet.AsTracking(); }
        }

        public virtual DbSet<T> DbSet
        {
            get { return Context.Set<T>(); }
        }

        public virtual async Task<int> CountAsync(FilterModel filter, bool isTenantSpecificData = false)
        {
            if (filter == null)
                return 0;

            // if (!filter.Predicates.Any(x => x.Key == "TenantId"))
            //     filter.Predicates.Add("TenantId", _appTenant.TenantId);
            var query = ApplyFilters(filter);
            return await query.CountAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAsync(FilterModel filter)
        {
            if (filter == null)
            {
                throw new ArgumentNullException(nameof(filter));
            }

            if (filter.Page == 0)
            {
                throw new ArgumentNullException(nameof(filter.Page));
            }

            if (filter.PerPage == 0)
            {
                throw new ArgumentNullException(nameof(filter.PerPage));
            }

            filter.ValidateAndSetDefault();

            // if (!filter.Predicates.Any(x => x.Key == "TenantId"))
            //     filter.Predicates.Add("TenantId", _appTenant.TenantId);
            var query = ApplyFilters(filter);

            if (!string.IsNullOrEmpty(filter.OrderBy))
            {
                string orderByClause = filter.Direction == "desc" ? $"{filter.OrderBy} desc" : filter.OrderBy;
                query = query.OrderBy(orderByClause);
            }
            else
            {
                string orderByClause = "createdat desc";
                query = query.OrderBy(orderByClause);
            }

            return await query.Skip((filter.Page - 1) * filter.PerPage).Take(filter.PerPage).ToListAsync();
        }

        public virtual async Task<T> GetAsync(long id) => await Query.FirstOrDefaultAsync(e => e.Id == id);

        public virtual async Task<T> GetAsyncNoTrack(long id)
        {
            return await DbSet.AsTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public virtual async Task<T> GetAsyncAsNoTrack(long id)
        {
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public virtual IQueryable<T> GetQuery(Dictionary<string, object> predicates)
        {
            var query = Query;
            if (predicates == null)
                predicates = new Dictionary<string, object>();

            var inlineQuery = string.Empty;
            var parameters = new List<object>();
            int parameterCounter = 0;
            var andConditions = new List<string>();

            foreach (var predicate in predicates)
            {
                if (predicate.Value.GetType() == typeof(Guid))
                    andConditions.Add($"{predicate.Key} = @" + parameterCounter++);
                else if (predicate.Value.GetType() == typeof(string))
                    andConditions.Add($"{predicate.Key}.ToLower().Contains(@" + parameterCounter++ + ".ToLower())");
                else if (predicate.Value.GetType() == typeof(bool))
                    andConditions.Add($"{predicate.Key} = @" + parameterCounter++);
                else
                    andConditions.Add($"{predicate.Key}Contains(@" + parameterCounter++ + ")");
                parameters.Add(predicate.Value);
            }

            andConditions.Add($"status =(@" + parameterCounter++ + ")");
            parameters.Add("ACTIVE");

            inlineQuery = string.Join(" and ", andConditions);
            query = query.Where(inlineQuery, parameters.ToArray());

            return query;
        }

        public virtual async Task<T> Update_InsertAsync(T entity)
        {
            var checkEntry = FindByIdAsync(entity.Id);

            if (checkEntry != null)
            {
                await InsertAsync(entity);
            }
            else
            {
                await UpdateAsync(entity);
            }

            return entity;
        }

        public virtual async Task<T> InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (typeof(T).GetProperties().Any(k => k.Name == "CreatedAt"))
                Context.Entry(entity).Property("CreatedAt").CurrentValue = DateTime.Now;

            await DbSet.AddAsync(entity);
            return entity;
        }

        public virtual async Task<bool> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (typeof(T).GetProperties().Any(k => k.Name == "LastModifiedAt"))
                Context.Entry(entity).Property("LastModifiedAt").CurrentValue = DateTime.Now;

            DbSet.Update(entity);
            return await Task.FromResult(true);
        }

        public virtual async Task<bool> DeleteAsync(long id)
        {
            return await DeleteAsync(await GetAsync(id));
        }

        public virtual async Task<bool> DeleteAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            DbSet.Remove(entity);
            return await Task.FromResult(true);
        }

        public virtual async Task<bool> SoftDeleteAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            

            var entry = Context.Entry(entity);

            foreach (var item in entry.CurrentValues.Properties.Where(x => x.Name != "Status"))
            {
                Context.Entry(entity).Property(item.Name).IsModified = false;
            }

           // entity.Status = "DELETED";

            DbSet.Update(entity);

            return await Task.FromResult(true);
        }


        public virtual async Task<bool> TenantFilterAny(
                      Expression<Func<T, bool>> filter,
                      Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (include != null)
                query = include(query);

            if (filter != null)
                query = query.Where(filter);

            return await query.AnyAsync();
        }

        public virtual async Task<List<T>> FindAllAsync(
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (include != null)
                query = include(query);

            if (selector != null)
                query = query.Select(selector);

            if (orderBy != null)
                query = orderBy(query);

            return await query.ToListAsync();
        }

        public virtual async Task<(List<T> Result, long TotalRecords)> FindAsync(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, T>>? selector = null,
            string? orderBy = null,
            string direction = "asc",
            int page = 1,
            int perPage = 10,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (filter != null)
                query = query.Where(filter);

            var count = await query.LongCountAsync();

            if (include != null)
                query = include(query);

            if (selector != null)
                query = query.Select(selector);

            if (orderBy != null)
            {
                //string orderByClause = direction == "desc" ? $"{orderBy} desc" : orderBy;
                //query = query.OrderBy(orderByClause);
            }
            else
            {
                //string orderByClause = " createdat desc";
                //query = query.OrderBy(orderByClause);
            }

            query = query.Skip((page - 1) * perPage).Take(perPage);

            return (await query.ToListAsync(), count);
        }

        public virtual async Task<List<T>> FindAsync(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            int? page = null,
            int? perPage = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (filter != null)
                query = query.Where(filter);

            if (include != null)
                query = include(query);

            if (selector != null)
                query = query.Select(selector);

            if (orderBy != null)
                query = orderBy(query);

            if (page.HasValue && perPage.HasValue)
                query = query.Skip((page.Value - 1) * perPage.Value).Take(perPage.Value);

            return await query.ToListAsync();
        }

        public virtual async Task<T> FindByIdAsync(
            long id,
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;
            query = query.Where(x => x.Id == id);

            if (include != null)
                query = include(query);

            if (selector != null)
                query = query.Select(selector);

            return await query.FirstOrDefaultAsync();
        }

        public virtual async Task<T> FirstOrDefaultAsync(
              Expression<Func<T, bool>> filter,
              Expression<Func<T, T>>? selector = null,
              Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (filter != null)
                query = query.Where(filter);

            if (include != null)
                query = include(query);

            if (selector != null)
                query = query.Select(selector);

            return await query.FirstOrDefaultAsync();
        }

        public virtual async Task<T> SingleOrDefaultAsync(
              Expression<Func<T, bool>> filter,
              Expression<Func<T, T>>? selector = null,
              Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (filter != null)
                query = query.Where(filter);

            if (include != null)
                query = include(query);

            if (selector != null)
                query = query.Select(selector);

            return await query.SingleOrDefaultAsync();
        }

        public virtual async Task<bool> Any(
              Expression<Func<T, bool>> filter,
              Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (include != null)
                query = include(query);

            if (filter != null)
                query = query.Where(filter);

            return await query.AnyAsync();
        }

        public virtual async Task<long> CountAsync(
               Expression<Func<T, bool>> filter,
               Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            var query = Query;

            if (include != null)
            {
                query = include(query);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.CountAsync();
        }

        public virtual async Task<bool> UpdateAsync(T entity, List<string> includes)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (typeof(T).GetProperties().Any(k => k.Name == "LastModifiedAt"))
                Context.Entry(entity).Property("LastModifiedAt").CurrentValue = DateTime.Now;

            DbSet.Update(entity);
            return await Task.FromResult(true);
        }

        private IQueryable<T> ApplyFilters(FilterModel filter)
        {
            var inlineQuery = string.Empty;
            var parameters = new List<object>();
            int parameterCounter = 0;



            if (filter.Predicates.Count() > 0)
            {
                var andConditions = new List<string>();
                foreach (var predicate in filter.Predicates)
                {
                    if (predicate.Key.ToString().Contains("!"))
                    {
                        andConditions.Add($"{predicate.Key}= @" + parameterCounter++);
                        parameters.Add(predicate.Value);
                    }
                    else
                    {
                        andConditions.Add($"{predicate.Key} = @" + parameterCounter++);
                        parameters.Add(predicate.Value);
                    }
                }

                inlineQuery = inlineQuery + " and (" + string.Join(" and ", andConditions) + ") ";
            }

            if (!string.IsNullOrEmpty(filter.SearchColumns) && !string.IsNullOrEmpty(filter.SearchValue))
            {
                var orConditions = new List<string>();
                string[] columns = filter.SearchColumns.Split(new[] { ',' });
                foreach (var column in columns)
                {
                    orConditions.Add($"{column}.Contains(@{parameterCounter})");
                }

                parameters.Add(filter.SearchValue);

                var orQuery = " (" + string.Join(" or ", orConditions) + ") ";
                if (!string.IsNullOrEmpty(inlineQuery))
                    inlineQuery = inlineQuery + " and ";

                inlineQuery = inlineQuery + orQuery;
                parameterCounter++;
            }

            //if (!string.IsNullOrEmpty(inlineQuery))
            //{
            //    inlineQuery = inlineQuery + " and status!=@" + parameterCounter.ToString();
            //    parameters.Add("DELETED");
            //}

            var query = Query;
            if (!string.IsNullOrEmpty(filter.Includes))
            {
                string[] includes = filter.Includes.Split(',');
                foreach (string include in includes)
                    query = query.Include(include);
            }

            if (!string.IsNullOrEmpty(inlineQuery))
                query = query.Where(inlineQuery, parameters.ToArray());

            return query;
        }

    }

}
