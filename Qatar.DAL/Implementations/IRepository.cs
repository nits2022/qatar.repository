﻿using Microsoft.EntityFrameworkCore.Query;
using Qatar.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Qatar.DAL.Implementations
{
    public interface IRepository<T>
        where T : Entity
    {
        // will be removed in future
        Task<IEnumerable<T>> GetAsync(FilterModel filter);

        // will be removed in future
        Task<int> CountAsync(FilterModel filter, bool isTenantSpecificData = false);

        IQueryable<T> GetQuery(Dictionary<string, object> predicates);

        Task<T> GetAsync(long id);

        Task<T> GetAsyncNoTrack(long id);

        Task<T> GetAsyncAsNoTrack(long id);

        Task<T> InsertAsync(T entity);

        Task<T> Update_InsertAsync(T entity);

        Task<bool> UpdateAsync(T entity);

        Task<bool> UpdateAsync(T entity, List<string> includes);

        Task<bool> DeleteAsync(long id);

        Task<bool> DeleteAsync(T entity);

        Task<bool> SoftDeleteAsync(T entity);

        // Can be used to fetch all data (e.g. master data)
        Task<List<T>> FindAllAsync(
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        // Used for binding grid data
        Task<(List<T> Result, long TotalRecords)> FindAsync(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, T>>? selector = null,
            string? orderBy = null,
            string direction = "asc",
            int page = 1,
            int perPage = 10,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<List<T>> FindAsync(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
            int? page = null,
            int? perPage = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<T> FindByIdAsync(
            long id,
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<T> FirstOrDefaultAsync(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<T> SingleOrDefaultAsync(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, T>>? selector = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<bool> Any(
            Expression<Func<T, bool>> filter,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<bool> TenantFilterAny(
            Expression<Func<T, bool>> filter,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<long> CountAsync(
            Expression<Func<T, bool>> filter,
            Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);
    }
}
