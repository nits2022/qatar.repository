﻿using Autofac;
using Autofac.Core;
using Microsoft.Extensions.Logging;
using Qatar.DAL.Context;
using Qatar.DAL.IRepositories;
using Qatar.DAL.Repositories;
using System.Collections.Generic;

namespace Qatar.DAL.InstallerModules
{
    public class RepositoryModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QatarDbContext>()
                                      .InstancePerLifetimeScope()
                                        .WithParameters(new List<Parameter>()
                                        {
                    // new ResolvedParameter(
                    // (x, c) => x.Name == "connectionString",
                    // (x, c) => c.Resolve<IOptions<RdsConfiguration>>().Value.ConnectionString),
                    new ResolvedParameter(
                        (p, c) => p.Name == "loggerFactory",
                        (p, c) => { return c.Resolve<ILoggerFactory>(); })
                                        });

            builder.RegisterType<EFContext>()
                    .As<IEFContext>()
                       .InstancePerLifetimeScope()
                       .WithParameters(new List<Parameter>()
                       {
                    new ResolvedParameter(
                        (p, c) => p.Name == "loggerFactory",
                        (p, c) => { return c.Resolve<ILoggerFactory>(); }),
                    new ResolvedParameter(
                        (p, c) => p.Name == "context",
                        (p, c) => c.Resolve<QatarDbContext>())
                       });

           

            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
           
        }

    }
}
