﻿using System.Collections.Generic;

namespace Qatar.DAL.Models
{
    public  class FilterModel
    {
        public const int DEFAULT_PER_PAGE = 10;

        public const int DEFAULT_PAGE = 1;

        public string OrderBy { get; set; } = string.Empty;

        public string Direction { get; set; } = "asc";

        public int Page { get; set; } = 0;

        public int PerPage { get; set; } = 0;

        public string SearchColumns { get; set; } = string.Empty;

        public string SearchValue { get; set; } = string.Empty;

        // /key = column name, value = search value
        public Dictionary<string, object> Predicates { get; set; } = new Dictionary<string, object>();

        public string Includes { get; set; } = string.Empty;

        public void ValidateAndSetDefault()
        {
            // Validate and set defualt paging
            Page = Page < 0 ? 0 : Page;
            PerPage = PerPage < 0 ? 0 : PerPage;

            if (Page > 0 && PerPage == 0)
            {
                PerPage = DEFAULT_PER_PAGE;
            }

            if (PerPage > 0 && Page == 0)
            {
                Page = DEFAULT_PAGE;
            }

            // Validate and set default search
            if (!string.IsNullOrEmpty(SearchValue) && string.IsNullOrEmpty(SearchColumns))
            {
                SearchValue = string.Empty;
            }
        }
    }
}
