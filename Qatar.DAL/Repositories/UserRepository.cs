﻿using Qatar.DAL.Context;
using Qatar.DAL.DataModel;
using Qatar.DAL.IRepositories;

namespace Qatar.DAL.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IEFContext context) : base(context)
        {

        }
    }
}
