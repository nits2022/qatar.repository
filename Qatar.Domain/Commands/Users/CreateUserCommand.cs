﻿using MediatR;
using Qatar.DAL.Handlers;
using Qatar.Domain.DTO;

namespace Qatar.Domain.Commands.Users
{
    public class CreateUserCommand : IRequest<UserDto>, ITransactionalRequest
    {
        public CreateUserCommand(UserDto request)
        {
            Request = request;
        }

        public UserDto Request { get; } = new UserDto();

        public UserDto Result { get; set; }
    }
}
