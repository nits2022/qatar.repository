﻿using CSharpFunctionalExtensions;
using MediatR;
using Qatar.DAL.Handlers;

namespace Qatar.Domain.Commands.Users
{
    public class DeleteUserCommand : IRequest<Result>, ITransactionalRequest
    {
        public DeleteUserCommand(long id)
        {
            Id = id;
        }

        public long Id { get; private set; }
    }
}
