﻿using CSharpFunctionalExtensions;
using MediatR;
using Qatar.DAL.Handlers;
using Qatar.Domain.DTO;

namespace Qatar.Domain.Commands.Users
{
    public class UpdateUserCommand : IRequest<Result<UserDto>>, ITransactionalRequest
    {
        public UpdateUserCommand(UserDto request)
        {
            Request = request;
        }

        public UserDto Request { get; set; }
    }
}
