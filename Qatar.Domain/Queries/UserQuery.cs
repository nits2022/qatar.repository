﻿using MediatR;
using Qatar.DAL.Models;
using Qatar.Domain.DTO;
using System.Collections.Generic;

namespace Qatar.Domain.Queries
{
    public class UserQuery : IRequest<(IEnumerable<UserDto> result, int count)>
    {
        public UserQuery(FilterModel filter, long? selectedId)
        {
            Filter = filter;
            SelectedId = selectedId;
        }

        public FilterModel Filter { get; set; }

        public long? SelectedId { get; set; }
    }
}
