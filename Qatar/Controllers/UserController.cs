﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Qatar.DAL.IRepositories;
using Qatar.DAL.Models;
using Qatar.Domain.Commands.Users;
using Qatar.Domain.DTO;
using Qatar.Domain.Queries;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Qatar.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;

        private readonly IUserRepository _userRepository;

        private readonly IMediator _mediator;

        private readonly IMapper _mapper;

        public UserController(ILogger<UserController> logger, IUserRepository userRepository, IMapper mapper,
            IMediator mediator)
        {
            _logger = logger;
            _userRepository = userRepository;
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet(Name = "User/GetUsers")]
        [SwaggerOperation(Summary = "GetUsersss", Description = "GetAll Description")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, description: "test", Type = typeof(List<UserDto>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> GetUserss() => Ok(await _userRepository.FindAllAsync());

        [HttpGet("GetDetail")]
        [SwaggerOperation("GetDetail", "GetUserDeatils")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]

        public async Task<ActionResult> GetDetail([FromQuery] FilterModel filter, [FromQuery] long? selectedId)
        {
            var result = await _mediator.Send(new UserQuery(filter, selectedId));
            Response.Headers.Add("TotalCount", result.count.ToString());
            return Ok(result.result);
        }

        [HttpGet("GetUserById")]
        [ProducesResponseType(typeof(UserDto), (int)HttpStatusCode.OK)]
        [SwaggerOperation("GetDetail", "GetUserDeatils")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> GetById(long id)
        {
            var item = await _userRepository.GetAsync(id);
            if (item == null)
                return NotFound();
            var result = _mapper.Map<UserDto>(item);
            return Ok(result);
        }

        [HttpPost]
        [SwaggerOperation("CreateUser")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> PostAsync([FromBody] UserDto user)
        {
            var command = new CreateUserCommand(user);
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult> PutAsync([FromRoute] long id, [FromBody] UserDto dto)
        {
            if (id == 0)
                return BadRequest($"Provided value for {nameof(id)} is not valid");

            dto.Id = id;
            var command = new UpdateUserCommand(dto);
            var result = await _mediator.Send(command);
            if (result.IsFailure)
                return BadRequest(result.Error);
            return Ok();
        }

        [HttpDelete(Name = "Delete")]
        public async Task<ActionResult> DeleteAsync(long id)
        {
            if (id == 0)
                return BadRequest($"Provided value for {nameof(id)} is not valid");
            var command = new DeleteUserCommand(id);
            var result = await _mediator.Send(command);
            if (result.IsFailure)
                return BadRequest(result.Error);
            return Ok();
        }
    }
}
