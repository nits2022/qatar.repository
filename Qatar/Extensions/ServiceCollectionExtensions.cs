﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Qatar.BAL.CommandModules;
using Qatar.DAL.InstallerModules;
using Qatar.Domain.Configurations;

namespace Qatar.WebAPI.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterModules(this ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(CommandModules).Assembly);
            builder.RegisterAssemblyModules(typeof(RepositoryModule).Assembly);
        }

        public static IServiceCollection RegisterConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RdsConfiguration>(configuration.GetSection("ConnectionStrings"));

            return services;
        }
    }
}
