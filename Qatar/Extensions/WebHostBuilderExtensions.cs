﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Qatar.Extensions
{
    public static class WebHostBuilderExtensions
    {
        public static IWebHostBuilder AddConfiguration(this IWebHostBuilder webHostBuilder, string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            return webHostBuilder
                .UseConfiguration(configuration)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                // var env = hostingContext.HostingEnvironment;
                // config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                //     .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                config.AddEnvironmentVariables();
                });
        }
    }
}
