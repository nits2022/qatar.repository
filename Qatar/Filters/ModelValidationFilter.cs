﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Qatar.WebAPI.Validators.Models;

namespace Qatar.WebAPI.Filters
{
    public class ModelValidationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            ErrorResponse errorResponse = null;
            if (!context.ModelState.IsValid)
            {
                errorResponse = new ErrorResponse(context.ModelState);
            }

            if (errorResponse != null)
            {
                context.HttpContext.Response.StatusCode = 400;
                context.Result = new JsonResult(errorResponse);
            }
        }
    }
}
