﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Qatar.WebAPI.Validators.Models;
using System.Net;

namespace Qatar.WebAPI.Filters
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public UnhandledExceptionFilter(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public override void OnException(ExceptionContext context)
        {
            var error = new Error(
                context.Exception.ToString(), "SomethingWentWrong");

            var trace = context.Exception.ToString();
            if (_hostingEnvironment.IsDevelopment())
                error.WithTrace(trace);

            HandleJsonResult(context, error, HttpStatusCode.InternalServerError);
        }

        private void HandleJsonResult(ExceptionContext context, Error error, HttpStatusCode statusCode)
        {
            var errorResponse = new ErrorResponse(error);
            errorResponse.Message = context.Exception.Message;
            JsonResult result = new JsonResult(errorResponse);
            context.HttpContext.Response.StatusCode = (int)statusCode;
            context.Result = result;
        }
    }
}
