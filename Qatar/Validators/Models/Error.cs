﻿using Newtonsoft.Json;

namespace Qatar.WebAPI.Validators.Models
{
    public class Error
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; }

        public string Message { get; }

        public string Trace { get; set; }

        public Error(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }

        public Error(string message)
        {
            Message = message;
        }

        public void WithTrace(string trace)
        {
            Trace = trace;
        }
    }
}
