﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace Qatar.WebAPI.Validators.Models
{
    public class ErrorResponse
    {
        public string Message { get; set; }

        public List<Error> Errors { get; }

        public ErrorResponse(ModelStateDictionary modelState)
        {
            Message = "Common.InvalidFormData";
            Errors = modelState.Keys
                    .SelectMany(key => modelState[key].Errors.Select(x => new Error(key, x.ErrorMessage)))
                    .ToList();
        }

        public ErrorResponse(string message, List<Error> errors)
        {
            Message = message;
            Errors = errors;
        }

        public ErrorResponse(string message, Error error)
        : this(error)
        {
            Message = message;
        }

        public ErrorResponse(Error error)
        {
            Errors = new List<Error> { error };
        }
    }
}
