﻿using FluentValidation;
using Qatar.DAL.IRepositories;
using Qatar.Domain.DTO;

namespace Qatar.WebAPI.Validators
{
    public class UserDtoValidator : AbstractValidator<UserDto>
    {
        public UserDtoValidator(IUserRepository userRepository)
        {
            RuleFor(p => p.FirstName).NotEmpty().WithMessage("Please EnterName").Length(1, 50).WithMessage("MaxLengthValidation50").MustAsync(async (dto, type, context) =>
            {
                var firstName = dto.FirstName.ToLower().Trim();
                var isValid = await userRepository.Any(filter: m => m.FirstName.ToLower().Trim() == firstName && m.Id != dto.Id);
                return !isValid;
            }).WithMessage("Name Already Exist");
        }
    }
}
